require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(:+).to_i
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method

def sub_string?(a_string, sub_string)
   a_string.split(' ').any?{|el|el == sub_string}
end

def in_all_strings?(long_strings, substring)
  long_strings.all?{|l_s|sub_string?(l_s, substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  s_chars = string.chars
  s_chars.select{|char|s_chars.count(char) > 1 && char =~ /[a-z]/}.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  result = []
  words = string.split(' ').sort_by(&:length)
  result << words.pop << words.pop
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reject{|l| string.chars.include?(l)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select{|yr|not_repeat_year?(yr)}
end


# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?

def no_repeats(song_name, songs)
  songs.each_cons(2).each do |a|
    return false if (a[0] == a[1]) && (a[0] == song_name)
  end
  return true
end

def one_week_wonders(songs)
  songs.select{|song_name| no_repeats(song_name, songs)}.uniq
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def remove_punctuation(str)
  str.delete("^ a-z")
end

def c_distance(word)
  word.include?("c") ? (word.length - word.reverse.index("c")) : 10
end

def for_cs_sake(string)
  return "" if !(string.include?("c"))
  arr1 = remove_punctuation(string).split(' ')
  arr2 = arr1.map{|w| c_distance(w)}
  idx = arr2.index(arr2.min)
  arr1[idx]
end




# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]


def repeated_number_ranges(arr)
  result = []
  start_idx = nil

  arr.each.with_index do |num, idx|
    if num == arr[idx+1]
      start_idx = idx unless start_idx
    elsif start_idx
      result << [start_idx, idx]
      start_idx = nil
    end
  end

  result
end
